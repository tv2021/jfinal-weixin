/*
 * Copyright (c) 2011-2021, L.cm (596392912@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.api;

import com.jfinal.weixin.sdk.utils.HttpUtils;
import com.jfinal.weixin.sdk.utils.JsonUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 草稿箱
 * <p>
 * 文档地址：https://developers.weixin.qq.com/doc/offiaccount/Draft_Box/Add_draft.html
 *
 * @author L.cm
 */
public class DraftBoxApi {
    public static final String API_PREFIX = "https://api.weixin.qq.com/cgi-bin/draft/";

    /**
     * 新建草稿
     *
     * @param articles 文章集合
     * @return ApiResult
     */
    public static ApiResult add(List<MediaArticles> articles) {
        String url = API_PREFIX + "add?access_token=" + AccessTokenApi.getAccessTokenStr();
        String jsonResult = HttpUtils.post(url, JsonUtils.toJson(articles));
        return new ApiResult(jsonResult);
    }

    /**
     * 更新草稿文章
     *
     * @param article 文章信息
     * @return ApiResult
     */
    public static ApiResult update(String mediaId, int index, MediaArticles article) {
        String url = API_PREFIX + "update?access_token=" + AccessTokenApi.getAccessTokenStr();

        Map<String, Object> data = new HashMap<>();
        data.put("media_id", mediaId);
        data.put("index", index);
        data.put("articles", article);

        String jsonResult = HttpUtils.post(url, JsonUtils.toJson(data));
        return new ApiResult(jsonResult);
    }

    /**
     * 获取草稿
     *
     * @param mediaId mediaId
     * @return ApiResult
     */
    public static ApiResult get(String mediaId) {
        String url = API_PREFIX + "get?access_token=" + AccessTokenApi.getAccessTokenStr();

        Map<String, String> data = new HashMap<>();
        data.put("media_id", mediaId);

        String jsonResult = HttpUtils.post(url, JsonUtils.toJson(data));
        return new ApiResult(jsonResult);
    }

    /**
     * 获取草稿
     *
     * @param offset offset
     * @param count count
     * @return ApiResult
     */
    public static ApiResult getBatch(int offset, int count) {
        return getBatch(offset, count, false);
    }

    /**
     * 获取草稿
     *
     * @param offset offset
     * @param count count 返回素材的数量，取值在1到20之间
     * @param noContent noContent
     * @return ApiResult
     */
    public static ApiResult getBatch(int offset, int count, boolean noContent) {
        String url = API_PREFIX + "batchget?access_token=" + AccessTokenApi.getAccessTokenStr();

        Map<String, Object> data = new HashMap<>();
        data.put("offset", offset);
        data.put("count", count);
        data.put("no_content", noContent ? 1 : 0);

        String jsonResult = HttpUtils.post(url, JsonUtils.toJson(data));
        return new ApiResult(jsonResult);
    }

    /**
     * 删除草稿
     *
     * @param mediaId mediaId
     * @return ApiResult
     */
    public static ApiResult delete(String mediaId) {
        String url = API_PREFIX + "delete?access_token=" + AccessTokenApi.getAccessTokenStr();

        Map<String, String> data = new HashMap<>();
        data.put("media_id", mediaId);

        String jsonResult = HttpUtils.post(url, JsonUtils.toJson(data));
        return new ApiResult(jsonResult);
    }

    /**
     * 获取草稿
     *
     * @return ApiResult
     */
    public static ApiResult count() {
        String url = API_PREFIX + "count?access_token=" + AccessTokenApi.getAccessTokenStr();

        String jsonResult = HttpUtils.get(url);
        return new ApiResult(jsonResult);
    }


}
